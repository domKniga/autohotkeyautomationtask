﻿using AutoHotkey.Interop;
using AutohotkeyAutomation.Model;
using AutohotkeyAutomation.Resources;
using AutohotkeyAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace AutohotkeyAutomation
{
    /// <summary>
    /// Window code-behind logic - implemented here due to small-scope requirements of client(very low chance of future extensions).
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The AutoHotkey engine to load the scripts in.
        /// </summary>
        protected readonly AutoHotkeyEngine AHK_ENGINE = AutoHotkeyEngine.Instance;

        protected readonly CombinedScript COMBINED_SCRIPT = new CombinedScript();
        protected readonly NavigationOnlyScript NAVIGATION_ONLY_SCRIPT = new NavigationOnlyScript();

        public MainWindow()
        {
            InitializeComponent();

            //Initial actions
            LoadUnitsOfMeasure();
            LoadCachedValues();
            LoadScriptPreview(docCombinedScript, COMBINED_SCRIPT.Rebuild());
            LoadScriptPreview(docNavigationOnlyScript, NAVIGATION_ONLY_SCRIPT.Rebuild());
        }

        /// <summary>
        /// Loads the standard time delay units of measure into the combo boxes and sets miliseconds as default.
        /// </summary>
        protected void LoadUnitsOfMeasure()
        {
            List<UnitOfMeasure> uomList = UnitOfMeasure.Time.All();

            cbCombinedScriptUoms.ItemsSource = uomList;
            cbCombinedScriptUoms.SelectedIndex = 0;
        }

        /// <summary>
        /// Loads the script preview texts.
        /// </summary>
        protected void LoadScriptPreview(FixedDocument previewDoc, string scriptCode)
        {
            PageContent pageContent = new PageContent();
            FixedPage fixedPage = new FixedPage();

            //Fill text box with script code.
            TextBox tbScriptCodePreview = new TextBox();
            tbScriptCodePreview.Background = new SolidColorBrush(Colors.LavenderBlush);

            tbScriptCodePreview.Width = previewDoc.DocumentPaginator.PageSize.Width;
            tbScriptCodePreview.Height = previewDoc.DocumentPaginator.PageSize.Height;

            tbScriptCodePreview.FontSize = 14;
            tbScriptCodePreview.FontFamily = new FontFamily("Courier");
            tbScriptCodePreview.FontWeight = FontWeights.Bold;

            tbScriptCodePreview.TextWrapping = TextWrapping.Wrap;
            tbScriptCodePreview.Text = scriptCode;
            tbScriptCodePreview.IsReadOnly = true;

            //Add the text box to the FixedPage
            fixedPage.Children.Add(tbScriptCodePreview);

            //Add the FixedPage to the PageContent 
            pageContent.Child = fixedPage;

            //Add the PageContent to the FixedDocument
            previewDoc.Pages.Add(pageContent);
        }

        /// <summary>
        /// Loads the cached academy code, if applicable.
        /// </summary>
        protected void LoadCachedValues()
        {
            string[] caches = FileService.ReadCaches();
            foreach (string cache in caches)
            {
                string[] cachedVarValues = cache.Trim().Split(char.Parse(FileService.CACHE_VALUE_DELIMITER));

                string scriptIdentifier = cachedVarValues[0];

                //if (string.Equals(scriptIdentifier, typeof(CombinedScript).Name) && cachedVarValues.Length == (COMBINED_SCRIPT.Replacements.Count + 1))
                if (string.Equals(scriptIdentifier, typeof(CombinedScript).Name))
                {
                    //Load cached values into replacements.
                    COMBINED_SCRIPT.Replacements[CombinedScript.VAR_ACADEMY_CODE] = cachedVarValues[1];
                    COMBINED_SCRIPT.Replacements[CombinedScript.VAR_COMBINED_SCRIPT_HOTKEY] = cachedVarValues[2];
                    COMBINED_SCRIPT.Replacements[CombinedScript.VAR_TIME_DELAY] = cachedVarValues[3];
                    SelectCachedUom(cachedVarValues[4]);

                    //Populate fields with cached values.
                    tbAcademyCode.Text = COMBINED_SCRIPT.Replacements[CombinedScript.VAR_ACADEMY_CODE];
                    tbCombinedScriptHotkey.Text = COMBINED_SCRIPT.Replacements[CombinedScript.VAR_COMBINED_SCRIPT_HOTKEY];
                    tbCombinedScriptDelay.Text = COMBINED_SCRIPT.Replacements[CombinedScript.VAR_TIME_DELAY];
                }
                else if (string.Equals(scriptIdentifier, typeof(NavigationOnlyScript).Name) && cachedVarValues.Length == (NAVIGATION_ONLY_SCRIPT.Replacements.Count + 1))
                {
                    //Load cached values into replacements.
                    NAVIGATION_ONLY_SCRIPT.Replacements[NavigationOnlyScript.VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY] = cachedVarValues[1];

                    //Populate fields with cached values.
                    tbNavigationOnlyScriptHotkey.Text = NAVIGATION_ONLY_SCRIPT.Replacements[NavigationOnlyScript.VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY];
                }

            }
        }

        /// <summary>
        /// Selects the cached uom in the combo box.
        /// </summary>
        /// <param name="cachedUom">The symbol(name) of the cached uom.</param>
        private void SelectCachedUom(string cachedUom)
        {
            foreach (UnitOfMeasure uom in cbCombinedScriptUoms.Items)
            {
                if (string.Equals(uom.Name, cachedUom))
                {
                    cbCombinedScriptUoms.SelectedItem = uom;
                }
            }
        }

        /// <summary>
        /// Load the full script into the AHK engine to be ready for execution.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void LoadFullScript(object sender, RoutedEventArgs e)
        {
            Button btnLoadScript = sender as Button;

            // Normal case - disable button upon loading of script
            btnLoadScript.IsEnabled = false;

            // Prevent overlapping of the AHK script with pre-existing one.
            AHK_ENGINE.Reset();

            // Ensure all fields have values set
            bool noEmptyFields = SurefireService.IsInputPresent(tbAcademyCode.Text)
                && SurefireService.IsInputPresent(tbCombinedScriptHotkey.Text)
                && SurefireService.IsInputPresent(tbNavigationOnlyScriptHotkey.Text);

            if (!noEmptyFields)
            {
                MessagingService.ShowError("Липсва въведена стойност за някое поле!");

                //Exceptional case - enable button if script loading doesn`t happen.
                btnLoadScript.IsEnabled = true;
                return;
            }

            string academyCode = tbAcademyCode.Text;
            string combinedScriptHotkey = tbCombinedScriptHotkey.Text;
            string navigationOnlyScriptHotkey = tbNavigationOnlyScriptHotkey.Text;

            try
            {
                string fullScript = BuildFullScript();

                await Task.Factory.StartNew(() =>
                {
                    AHK_ENGINE.LoadScript(fullScript);
                    MessagingService.ShowInfo("Успешно зареждане на скрипа, комбинациите [" + combinedScriptHotkey + " , " + navigationOnlyScriptHotkey + "] са готови за ползване!");
                });
            }
            catch (ArgumentException argExc)
            {
                FileService.Log(DateTime.Now + " :: [" + argExc.GetType().Name + "] :: " + argExc.Message + "\n");
                MessagingService.ShowError("Скриптът не успя да се зареди с подадените параметри, моля опитай с други!");

                //Exceptional case - enable button if script loading fails.
                btnLoadScript.IsEnabled = true;
            }
            catch (Exception exc)
            {
                FileService.Log(DateTime.Now + " :: [" + exc.GetType().Name + "] :: " + exc.Message + "\n");
                MessagingService.ShowError("Скриптът не успя да се зареди, моля опитай пак или се свържи с програмиста!");

                //Exceptional case - enable button if script loading fails.
                btnLoadScript.IsEnabled = true;
            }
        }

        /// <summary>
        /// Build the full script comprising of specified sub-script templates.
        /// </summary>
        /// <returns>The full script to be loaded for execution.</returns>
        protected string BuildFullScript()
        {
            StringBuilder fullScriptBuilder = new StringBuilder();

            string academyCode = tbAcademyCode.Text.Trim();
            string combinedScriptHotkey = tbCombinedScriptHotkey.Text.Trim();
            string navigationOnlyScriptHotkey = tbNavigationOnlyScriptHotkey.Text.Trim();

            string combinedScriptDelay = tbCombinedScriptDelay.Text.Trim();

            int combinedScriptDelayFactor = (cbCombinedScriptUoms.SelectedItem as UnitOfMeasure).Factor;
            int combinedScriptDelayCalculated = SurefireService.SafeGetValue(combinedScriptDelay, 0) * combinedScriptDelayFactor;

            string academyCodeViolations = string.Empty;

            bool codeValid = SurefireService.IsAcademyCodeValid(academyCode, out academyCodeViolations);
            if (!codeValid)
            {
                MessagingService.ShowError(academyCodeViolations);
                throw new ArgumentException("Invalid input for academy code: " + academyCode);
            }

            // Combined script rebuild.
            COMBINED_SCRIPT.Replacements[CombinedScript.VAR_ACADEMY_CODE] = academyCode;
            COMBINED_SCRIPT.Replacements[CombinedScript.VAR_COMBINED_SCRIPT_HOTKEY] = combinedScriptHotkey;
            COMBINED_SCRIPT.Replacements[CombinedScript.VAR_TIME_DELAY] = combinedScriptDelayCalculated.ToString();

            string combinedScriptCode = COMBINED_SCRIPT.Rebuild();
            FileService.Log(DateTime.Now + " :: [COMBINED SCRIPT] : \n" + combinedScriptCode + "\n");

            //Navigation-only script rebuild.
            NAVIGATION_ONLY_SCRIPT.Replacements[NavigationOnlyScript.VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY] = navigationOnlyScriptHotkey;

            string navigationOnlyScriptCode = NAVIGATION_ONLY_SCRIPT.Rebuild();
            FileService.Log(DateTime.Now + " :: [NAVIGATION-ONLY SCRIPT] : \n" + navigationOnlyScriptCode + "\n");

            // Full script log
            fullScriptBuilder
                .Append(combinedScriptCode)
                .AppendLine()
                .Append(navigationOnlyScriptCode);
            FileService.Log(DateTime.Now + " :: [FULL SCRIPT] : \n" + fullScriptBuilder.ToString() + "\n");

            return fullScriptBuilder.ToString();
        }

        private void tbCombinedScriptHotkey_KeyUp(object sender, KeyEventArgs e)
        {
            CaptureAhkHotkey(sender as TextBox, e);
        }

        private void tbNavigationOnlyScriptHotkey_KeyUp(object sender, KeyEventArgs e)
        {
            CaptureAhkHotkey(sender as TextBox, e);
        }

        protected void CaptureAhkHotkey(TextBox captureFieldToUpdate, KeyEventArgs e)
        {
            string hotkey = string.Empty;

            if (!SupportedHotkeys.AHK_HOTKEYS.TryGetValue(e.Key, out hotkey))
            {
                SupportedHotkeys.AHK_HOTKEYS.TryGetValue(e.SystemKey, out hotkey);
            }

            captureFieldToUpdate.Text = SurefireService.SafeGetValue(hotkey, string.Empty);
        }

        private void tbAcademyCode_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateScriptPreview(sender as TextBox, docCombinedScript, COMBINED_SCRIPT, CombinedScript.VAR_ACADEMY_CODE);
        }

        private void tbCombinedScriptHotkey_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateScriptPreview(sender as TextBox, docCombinedScript, COMBINED_SCRIPT, CombinedScript.VAR_COMBINED_SCRIPT_HOTKEY);
        }

        private void tbNavigationOnlyScriptHotkey_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateScriptPreview(sender as TextBox, docNavigationOnlyScript, NAVIGATION_ONLY_SCRIPT, NavigationOnlyScript.VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY);
        }

        private void tbCombinedScriptDelay_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateScriptPreview(sender as TextBox, docCombinedScript, COMBINED_SCRIPT, CombinedScript.VAR_TIME_DELAY);
        }

        protected void UpdateScriptPreview(TextBox fieldWithValue, FixedDocument documentToUpdate, IAhkScript scriptToUpdate, string varToReplace)
        {
            string changedValue = fieldWithValue.Text;
            if (!string.IsNullOrEmpty(changedValue))
            {
                TextBox docPreviewTextBox = documentToUpdate.Pages[0].Child.Children.OfType<TextBox>().FirstOrDefault();

                scriptToUpdate.Replacements[varToReplace] = changedValue;

                docPreviewTextBox.Text = scriptToUpdate.Rebuild();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Cache values
            bool noEmptyFields = SurefireService.IsInputPresent(tbAcademyCode.Text)
                && SurefireService.IsInputPresent(tbCombinedScriptHotkey.Text)
                && SurefireService.IsInputPresent(tbNavigationOnlyScriptHotkey.Text);

            if (noEmptyFields)
            {
                StringBuilder cacheBuilder = new StringBuilder();

                cacheBuilder
                    .Append(typeof(CombinedScript).Name).Append(FileService.CACHE_VALUE_DELIMITER)
                    .Append(tbAcademyCode.Text.Trim()).Append(FileService.CACHE_VALUE_DELIMITER)
                    .Append(tbCombinedScriptHotkey.Text.Trim()).Append(FileService.CACHE_VALUE_DELIMITER)
                    .Append(SurefireService.SafeGetValue(tbCombinedScriptDelay.Text.Trim(), 0.ToString())).Append(FileService.CACHE_VALUE_DELIMITER)
                    .Append((cbCombinedScriptUoms.SelectedValue as UnitOfMeasure).Name)

                    .AppendLine()

                    .Append(typeof(NavigationOnlyScript).Name).Append(FileService.CACHE_VALUE_DELIMITER)
                    .Append(tbNavigationOnlyScriptHotkey.Text.Trim());

                bool cacheSuccess = FileService.Cache(cacheBuilder.ToString());
                if (!cacheSuccess)
                {
                    MessagingService.ShowWarning("Запазването на въведените стойности беше неуспешно!");
                }
            }

            // Purge log if necessary
            bool purgeLogSuccess = FileService.TryPurgeLog();
            if (!purgeLogSuccess)
            {
                MessagingService.ShowWarning("Изтриването на лог файла беше неуспешно! \n Заеманото пространство е: " + FileService.GetLogSize());
            }
        }
    }
}
