﻿using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AutohotkeyAutomation.Model
{
    /// <summary>
    /// Combined script using AutoHotkey.
    /// </summary>
    public class CombinedScript : IAhkScript
    {
        public const string VAR_COMBINED_SCRIPT_HOTKEY = "${VAR_COMBINED_SCRIPT_HOTKEY}";
        public const string VAR_ACADEMY_CODE = "${VAR_ACADEMY_CODE}";
        public const string VAR_TIME_DELAY = "${VAR_TIME_DELAY}";

        public StringBuilder Code
        {
            get
            {
                return new StringBuilder(VAR_COMBINED_SCRIPT_HOTKEY + "::"
                                                         + "\n Send, " + VAR_ACADEMY_CODE
                                                         + "\n Sleep, " + VAR_TIME_DELAY
                                                         + "\n Send, {tab}"
                                                         + "\n Sleep, 0"
                                                         + "\n Send, {enter}"
                                                         + "\n return");
            }
        }

        /// <summary>
        /// <para>Key: replacement variable name</para>
        /// <para>Value: replacment variable value</para>
        /// <para>In this case the backing dictionary uses the variable name as default value.</para>
        /// </summary>
        private IDictionary<string, string> _replacements = new Dictionary<string, string>()
        {
            {VAR_ACADEMY_CODE, VAR_ACADEMY_CODE },
            {VAR_COMBINED_SCRIPT_HOTKEY, VAR_COMBINED_SCRIPT_HOTKEY },
            {VAR_TIME_DELAY, VAR_TIME_DELAY }
        };
        public IDictionary<string, string> Replacements
        {
            get
            {
                return _replacements;
            }
        }

        public string Rebuild()
        {
            StringBuilder scriptSnapshot = new StringBuilder(Code.ToString());

            Replacements.ToList().ForEach(pair => scriptSnapshot.Replace(pair.Key, pair.Value));

            return scriptSnapshot.ToString();
        }
    }
}
