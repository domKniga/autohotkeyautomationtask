﻿using System.Text;
using System.Collections.Generic;

namespace AutohotkeyAutomation.Model
{
    /// <summary>
    /// Blueprint of an AutoHotkey script.
    /// </summary>
    public interface IAhkScript
    {
        StringBuilder Code { get; }

        /// <summary>
        /// <para>Key: replacement variable name</para>
        /// <para>Value: replacment variable value</para>
        /// </summary>
        IDictionary<string, string> Replacements { get; }

        /// <summary>
        /// Executes the replacement of values in the script code.
        /// </summary>
        /// <returns>The completed script code.</returns>
        string Rebuild();
    }
}
