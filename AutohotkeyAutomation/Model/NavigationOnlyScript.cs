﻿using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AutohotkeyAutomation.Model
{
    /// <summary>
    /// Navigation-Only script using AutoHotkey.
    /// </summary>
    public class NavigationOnlyScript : IAhkScript
    {
        public const string VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY = "${VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY}";

        public StringBuilder Code
        {
            get
            {
                return new StringBuilder(VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY + "::"
                                                         + "\n Send, {tab}"
                                                         + "\n Sleep, 0"
                                                         + "\n Send, {enter}"
                                                         + "\n return");
            }
        }

        /// <summary>
        /// <para>Key: replacement variable name</para>
        /// <para>Value: replacment variable value</para>
        /// <para>In this case the backing dictionary uses the variable name as default value.</para>
        /// </summary>
        private IDictionary<string, string> _replacements = new Dictionary<string, string>()
        {
            {VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY, VAR_NAVIGATION_ONLY_SCRIPT_HOTKEY }
        };
        public IDictionary<string, string> Replacements
        {
            get
            {
                return _replacements;
            }
        }

        public string Rebuild()
        {
            StringBuilder scriptSnapshot = new StringBuilder(Code.ToString());

            Replacements.ToList().ForEach(pair => scriptSnapshot.Replace(pair.Key, pair.Value));

            return scriptSnapshot.ToString();
        }
    }
}
