﻿using System.Windows.Input;
using System.Collections.Generic;

namespace AutohotkeyAutomation.Resources
{
    /// <summary>
    /// Static holder for the currently supported keys to use in AutoHotkey shortcuts.
    /// </summary>
    public static class SupportedHotkeys
    {
        /// <summary>
        /// Supported keys for AutoHotkey shortcuts.
        /// </summary>
        public static IDictionary<Key, string> AHK_HOTKEYS = new Dictionary<Key, string>()
        {
             //Alpha
             {Key.A, "A"},
             {Key.B, "B"},
             {Key.C, "C"},
             {Key.D, "D"},
             {Key.E, "E"},
             {Key.F, "F"},
             {Key.G, "G"},
             {Key.H, "H"},
             {Key.I, "I"},
             {Key.J, "J"},
             {Key.K, "K"},
             {Key.L, "L"},
             {Key.M, "M"},
             {Key.N, "N"},
             {Key.O, "O"},
             {Key.P, "P"},
             {Key.Q, "Q"},
             {Key.R, "R"},
             {Key.S, "S"},
             {Key.T, "T"},
             {Key.U, "U"},
             {Key.V, "V"},
             {Key.W, "W"},
             {Key.X, "X"},
             {Key.Y, "Y"},
             {Key.Z, "Z"},

             //Numeric
             {Key.D0, "0"},
             {Key.D1, "1"},
             {Key.D2, "2"},
             {Key.D3, "3"},
             {Key.D4, "4"},
             {Key.D5, "5"},
             {Key.D6, "6"},
             {Key.D7, "7"},
             {Key.D8, "8"},
             {Key.D9, "9"},

             //Function
             {Key.F1, "F1"},
             {Key.F2, "F2"},
             {Key.F3, "F3"},
             {Key.F4, "F4"},
             {Key.F5, "F5"},
             {Key.F6, "F6"},
             {Key.F7, "F7"},
             {Key.F8, "F8"},
             {Key.F9, "F9"},
             {Key.F10, "F10"},
             {Key.F11, "F11"},
             {Key.F12, "F12"},
        };
    }
}
