﻿using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace AutohotkeyAutomation.Resources
{
    /// <summary>
    /// Unit of measure representation(for script time delays)
    /// </summary>
    public class UnitOfMeasure
    {
        public string Name { get; }
        public int Factor { get; }

        /// <summary>
        /// Construct using the SI name of the unit of measure and a conversion factor(relative to Miliseconds).
        /// </summary>
        /// <param name="name"></param>
        /// <param name="factor"></param>
        public UnitOfMeasure(string name, int factor)
        {
            Name = name;
            Factor = factor;
        }

        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Standard units of measure implementations.
        /// </summary>
        public static class Time
        {
            public static readonly UnitOfMeasure MILISECONDS = new UnitOfMeasure("ms",1);
            public static readonly UnitOfMeasure SECONDS = new UnitOfMeasure("s", 1000);

            /// <summary>
            /// Provides a listing of all available time units of measure.
            /// </summary>
            /// <returns></returns>
            public static List<UnitOfMeasure> All()
            {
                List<UnitOfMeasure> uomList = new List<UnitOfMeasure>();

                FieldInfo[] uomConstants = typeof(Time).GetFields(BindingFlags.Static | BindingFlags.Public);
                uomConstants.ToList().ForEach(uom => uomList.Add(uom.GetValue(null) as UnitOfMeasure));

                return uomList;
            }
        }
    }
}
