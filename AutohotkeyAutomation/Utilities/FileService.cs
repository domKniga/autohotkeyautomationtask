﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace AutohotkeyAutomation.Utilities
{
    /// <summary>
    /// Service handling file operations.
    /// </summary>
    public class FileService
    {
        public const string CACHE_DELIMITER = "\n";
        public const string CACHE_VALUE_DELIMITER = ",";

        private const string CACHE_FILE = @"Resources\cache.txt";
        private const string LOGGER_FILE = "LOG.txt";

        private static readonly double MB_CONVERSION_FACTOR = Math.Pow(2, 20);
        private static readonly long FILE_LIMIT_SIZE = 5 * (long)MB_CONVERSION_FACTOR;

        private FileService() { }

        /// <summary>
        /// Reads the cache entries.
        /// </summary>
        /// <returns>An array of all cache entries.</returns>
        public static string[] ReadCaches()
        {
            if (File.Exists(CACHE_FILE))
            {
                try
                {
                    return File.ReadAllLines(CACHE_FILE, Encoding.UTF8);
                }
                catch (Exception)
                {
                    //Silently pass this block and return NULL (cache reading failure must not be fatal).
                }
            }

            return null;
        }

        /// <summary>
        /// Caches the input values.
        /// </summary>
        /// <param name="cacheContent">The built up string of lines(entries) to cache.</param>
        /// <returns>TRUE for success, FALSE otherwise.</returns>
        public static bool Cache(string cacheContent)
        {
            try
            {
                File.WriteAllText(CACHE_FILE, cacheContent, Encoding.UTF8);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Logs a message.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void Log(string message)
        {
            try
            {
                File.AppendAllText(LOGGER_FILE, message);
            }
            catch (Exception)
            {
                //Silently pass this block (logging failure must not be fatal).
            }
        }

        /// <summary>
        /// Checks whether the log file exceeds a certain size limit, purging the log if necessary.
        /// </summary>
        /// <returns>TRUE for success or if no purging was needed, FALSE otherwise</returns>
        public static bool TryPurgeLog()
        {
            // Delete log file if it gets too massive.
            FileInfo logInfo = GetLogInfo();
            if (logInfo != null)
            {
                long logSize = logInfo.Length;

                if (logSize >= FILE_LIMIT_SIZE)
                {
                    try
                    {
                        logInfo.Delete();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static FileInfo GetLogInfo()
        {
            if (File.Exists(LOGGER_FILE))
            {
                try
                {
                    return new FileInfo(LOGGER_FILE);
                }
                catch (Exception)
                {
                    //Silently pass this block and return NULL (log size lookup failure must not be fatal).
                }
            }

            return null;
        }

        /// <summary>
        /// Obtains the size of the log file.
        /// </summary>
        /// <returns>The number of bytes(long) representing the log size.</returns>
        public static long GetLogSize()
        {
            FileInfo logInfo = GetLogInfo();
            if(logInfo != null)
            {
                return logInfo.Length;
            }

            return 0L;
        }
    }
}
