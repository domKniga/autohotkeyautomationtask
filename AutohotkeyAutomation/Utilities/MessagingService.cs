﻿using System.Windows;

namespace AutohotkeyAutomation.Utilities
{
    /// <summary>
    /// Service handling simple message propagation for INFO / WARNING / ERROR messages.
    /// </summary>
    public class MessagingService
    {
        private MessagingService() { }

        /// <summary>
        /// Displays a message styled as INFORMATION.
        /// </summary>
        /// <param name="informationMessage">The information to display.</param>
        public static void ShowInfo(string informationMessage)
        {
            MessageBox.Show(informationMessage, "ИНФОРМАЦИЯ", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Displays a message styled as WARNING.
        /// </summary>
        /// <param name="warningMessage">The warning to display.</param>
        public static void ShowWarning(string warningMessage)
        {
            MessageBox.Show(warningMessage, "ПРЕДУПРЕЖДЕНИЕ", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// Displays a message styled as ERROR.
        /// </summary>
        /// <param name="errorMessage">The error to display.</param>
        public static void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage, "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

}
