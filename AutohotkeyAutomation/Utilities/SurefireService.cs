﻿using System;
using System.Text;
using System.Linq;
using System.Reflection;

namespace AutohotkeyAutomation.Utilities
{
    /// <summary>
    /// Service handling simple validation and safe value retrieval.
    /// </summary>
    public class SurefireService
    {
        public const int ACADEMY_CODE_LENGTH = 4;

        private SurefireService() { }

        /// <summary>
        /// Validates the pressence of value in a text field.
        /// </summary>
        /// <param name="textInput">The text of the text field.</param>
        /// <returns></returns>
        public static bool IsInputPresent(string textInput)
        {
            if (string.IsNullOrEmpty(textInput) || string.IsNullOrWhiteSpace(textInput))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates the format of input value of academy code text field.
        /// </summary>
        /// <param name="academyCodeInput">The text of the academy text field.</param>
        /// <param name="errors">String representation of all academy code format violations.</param>
        /// <returns>TRUE for success, FALSE otherwise as well as all academy code format violations, if applicable.</returns>
        public static bool IsAcademyCodeValid(string academyCodeInput, out string errors)
        {
            StringBuilder errorReportBuilder = new StringBuilder();

            if (academyCodeInput.Length != 4)
            {
                errorReportBuilder.Append("Неправилна дължината на въведения код ( очакват се 4 цифри )!").AppendLine();
            }

            if (academyCodeInput.ToCharArray().Any(character => !char.IsDigit(character)))
            {
                errorReportBuilder.Append("Неправилен формат на въведения код ( очаква се числова стойност )!").AppendLine();
            }

            errors = errorReportBuilder.ToString();

            return (errors.Length == 0);
        }

        /// <summary>
        /// Processes & obtains the value to be set, parsed if necessary or the fallback value in exceptional cases.
        /// </summary>
        /// <param name="inputValue">The input value of a text field.</param>
        /// <param name="fallbackValue">The fallback value of the expected data type.</param>
        /// <returns>The value to be set, parsed if necessary or the fallback value if necessary.</returns>
        public static T SafeGetValue<T>(string inputValue, T fallbackValue)
        {
            if (fallbackValue is string)
            {
                return (string.IsNullOrEmpty(inputValue) ? fallbackValue : (T)Convert.ChangeType(inputValue, typeof(T)));
            }
            else
            {
                Type expectedValueType = fallbackValue.GetType();
                MethodInfo parseMethod = expectedValueType.GetMethod("Parse", new Type[] { typeof(string) }, new ParameterModifier[] { new ParameterModifier(1) });

                T parsedValue = default(T);
                try
                {
                    object classInstance = Activator.CreateInstance(typeof(T), null);
                    parsedValue = (T)parseMethod.Invoke(classInstance, new object[] { inputValue });
                }
                catch (Exception)
                {
                    return fallbackValue;
                }

                return parsedValue;
            }
        }

    }
}
